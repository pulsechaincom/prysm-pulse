package params

import "math"

// UsePulseChainNetworkConfig uses the PulseChain beacon chain mainnet network config.
func UsePulseChainNetworkConfig() {
	cfg := BeaconNetworkConfig().Copy()
	cfg.ContractDeploymentBlock = 17233000
	cfg.BootstrapNodes = []string{
		"enr:-Ly4QLDmh67qBvQ8upk7WJ1VkbqkG1x82vwCcuAVBAxSadm9BL2rBVY98C5S3eWEOBwgo4eGESDT9YpiSCa7hlZW7ukDh2F0dG5ldHOIAAAABgAAAACEZXRoMpCEXuUqAAADbP__________gmlkgnY0gmlwhAUJfPSJc2VjcDI1NmsxoQOEQlTdMvl-ZVXW271J2bHTeVQsB9Uw0neBLjJMmxG_U4hzeW5jbmV0cwCDdGNwgiMog3VkcIIjKA",         // bootnode-001-hetzner-fsn
		"enr:-Ly4QMrEACp1pYBUZrPY7In4DrewwKBQugR-NIurEpV93p38WgNmbXVimfDyqsVICzI0YoNiGduG7TvriE-TVdYh1cIDh2F0dG5ldHOIMAAAAAAAAACEZXRoMpCEXuUqAAADbP__________gmlkgnY0gmlwhJT7Nt6Jc2VjcDI1NmsxoQN6-YmfTGSJYuOHNFje6cqBQs0VilzZyx5LKVTWasofpIhzeW5jbmV0cwCDdGNwgiMog3VkcIIjKA",         // bootnode-002-hetzner-fsn
		"enr:-Ly4QDvPDHVb59YfRdX258yeTpjsNz6c1mldDxJbdaS0KROFKSCFn9wWr0O8chfwUkCW5_K5lTzepegCjJ9lyNuHkHcDh2F0dG5ldHOIMAAAAAAAAACEZXRoMpCEXuUqAAADbP__________gmlkgnY0gmlwhEFs7OeJc2VjcDI1NmsxoQN6-YmfTGSJYuOHNFje6cqBQs0VilzZyx5LKVTWasofpIhzeW5jbmV0cwCDdGNwgiMog3VkcIIjKA",         // bootnode-003-hetzner-hel
		"enr:-Ly4QMYAm7CW8HJxG2h1EJ6kWz2VtHaPvim0XIlDiOGMtd1OZHzEKupdruIGV9Z-risnCr1VpCCOnbxm-O_4A6cIobUDh2F0dG5ldHOIAAYAAAAAAACEZXRoMpCEXuUqAAADbP__________gmlkgnY0gmlwhIe11OSJc2VjcDI1NmsxoQJznGdv7W7PrxXijqTeY9GDR74pwqj_6zPCynIj8Z2qoYhzeW5jbmV0cwCDdGNwgiMog3VkcIIjKA",         // bootnode-004-hetzner-hel
		"enr:-Ly4QEPEkeE3HaYDCX48HgZtONCEbM3rp5kWPz8okJl57AhLXFJAlZIqxMRrVj79SRi8EGJLxAAWcBj7vcSLLb0WSmQDh2F0dG5ldHOIAAAGAAAAAACEZXRoMpCEXuUqAAADbP__________gmlkgnY0gmlwhIe15bSJc2VjcDI1NmsxoQJ00SyAnKOc41sp2Fqbz4DNFgQKbu-SkLcgUWZ9eLKhUIhzeW5jbmV0cwCDdGNwgiMog3VkcIIjKA",         // bootnode-001 maintained by www.g4mm4.io
		"enr:-Ly4QFRmsOVOIE3cJ7lt0vMLNlW6aBPPlk_5Hbx-xRJSV54tX7XDE3hrkFeK9vtG35j83B9SBrOtTbQ-QDgBNUbvGuQDh2F0dG5ldHOIAAAAAAAAAMCEZXRoMpCEXuUqAAADbP__________gmlkgnY0gmlwhC4E4J-Jc2VjcDI1NmsxoQJSy4mez61R8W6CxeYsrdaEieQvGsSOcxexCeHowYKzHYhzeW5jbmV0cwCDdGNwgiMog3VkcIIjKA",         // bootnode-002 maintained by www.g4mm4.io
		"enr:-Ly4QDDiQOr-J-n6tZ_ZJkCTa2YGefZqNLC7aLvOtcrjNby9BHwmW5jTXPvdIn1w8k4ATseyCs7CUka2MnQ5TPW2WnADh2F0dG5ldHOIAAAAMAAAAACEZXRoMpCEXuUqAAADbP__________gmlkgnY0gmlwhC4E4KCJc2VjcDI1NmsxoQOmuaPCtAr87P3V7GWYq3mAffXP2mTnL7almJ9fyGlQ3YhzeW5jbmV0cwCDdGNwgiMog3VkcIIjKA",         // bootnode-003 maintained by www.g4mm4.io
		"enr:-MK4QHn-sOcSyQIlzT671o8_c0EYAGjZmqI177vaXh_bFZZqOk-Ln4MgznRyhdRyxfYuFaNgDqZlhPyu7FRxRpcx4oqGAY2Kdvkbh2F0dG5ldHOIAAAAAAAAAACEZXRoMpCEXuUqAAADbP__________gmlkgnY0gmlwhF_ZlOmJc2VjcDI1NmsxoQLJNe8yRLujxNigCBQh-Nw94waWaSQEan-7lCYkQmY_kYhzeW5jbmV0cwCDdGNwgjLIg3VkcIIu4A", // bootnode-004 maintained by www.g4mm4.io
		"enr:-MK4QJ0lyY1Z2dQ9RxQ4FhmfCPfDBoNuRHH26QeGzn99ZVr2ACc4gu-StMzXoPBaiDf04xy2Zwo6_dai6J5VIBURvFuGAY2KesZTh2F0dG5ldHOIAAAAAAAAAACEZXRoMpCEXuUqAAADbP__________gmlkgnY0gmlwhJT7uTSJc2VjcDI1NmsxoQMs-dti45Pz-Tbp4njHSBmBx2O4K3Ic8tDuFmJt3cs1PIhzeW5jbmV0cwCDdGNwgjLIg3VkcIIu4A", // bootnode-005 maintained by www.g4mm4.io
		"enr:-MK4QJ0-FjvHZnuUWRF8M3eLx_GFwyGkOajw5GxXB1UmAub9cgS2kLmXJ8v4fgpk7Za6WbHOPAKdve9xqT2n3z7uJ9OGAY2KfoKxh2F0dG5ldHOIAAAAAAAAAACEZXRoMpCEXuUqAAADbP__________gmlkgnY0gmlwhIrJ3DSJc2VjcDI1NmsxoQNTJDpeG0Su2BNXKbGzkE5XFONpZfnYd222qnQZ97qYSohzeW5jbmV0cwCDdGNwgjLIg3VkcIIu4A", // bootnode-006 maintained by www.g4mm4.io
	}
	OverrideBeaconNetworkConfig(cfg)
}

// PulseChainConfig defines the config for the PulseChain beacon chain mainnet.
func PulseChainConfig() *BeaconChainConfig {
	cfg := MainnetConfig().Copy()
	cfg.ConfigName = PulseChainName
	cfg.PresetBase = "pulsechain"

	// preset overrides
	cfg.BaseRewardFactor = 64000
	cfg.EffectiveBalanceIncrement = 1 * 1e15
	cfg.MaxEffectiveBalance = 32 * 1e15

	// config overrides
	cfg.TerminalTotalDifficulty = "58750003716598352947541"
	cfg.MinGenesisActiveValidatorCount = 4096
	cfg.MinGenesisTime = 1683776400
	cfg.GenesisForkVersion = []byte{0x00, 0x00, 0x03, 0x69}
	cfg.GenesisDelay = 300
	cfg.AltairForkVersion = []byte{0x00, 0x00, 0x03, 0x6a}
	cfg.AltairForkEpoch = 1
	cfg.BellatrixForkVersion = []byte{0x00, 0x00, 0x03, 0x6b}
	cfg.BellatrixForkEpoch = 2
	cfg.CapellaForkVersion = []byte{0x00, 0x00, 0x03, 0x6c}
	cfg.CapellaForkEpoch = 3
	cfg.DenebForkVersion = []byte{0x00, 0x00, 0x03, 0x6d}
	cfg.DenebForkEpoch = math.MaxUint64
	cfg.SecondsPerSlot = 10
	cfg.EjectionBalance = 16 * 1e15
	cfg.DepositChainID = 369
	cfg.DepositNetworkID = 369
	cfg.DepositContractAddress = "0x3693693693693693693693693693693693693693"

	cfg.InitializeForkSchedule()
	return cfg
}
